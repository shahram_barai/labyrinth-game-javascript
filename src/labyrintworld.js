class LabyrinthWorld {
    constructor(width, height, square_size) {
        this.grid = [];
        this.stack = []; // for backtracking
        this.longest_path = [];

        this.cols = Math.round(width / square_size) - 1;
        this.rows = Math.round(height / square_size) - 1;
        this.square_size = square_size;

        this.create_grid();

        this.start_square = this.grid[0]; // Start from the top left corner of the grid
        this.exit_square = this.grid[0];

        this.player = new Player("bob", this);
        this.player.set_location(this.start_square);
        this.nextSq;

        this.stack.push(this.start_square);
        this.mazeReady = false;
    }

    get_rows() { return this.rows; }
    get_cols() { return this.cols; }
    get_grid_size() { return this.grid.length; }
    get_square_size() { return this.square_size; }
    get_start_square() { return this.start_square; }
    get_exit_square() { return this.exit_square; }
    get_player_location() { return this.player.get_location(); }

    create_grid() {
        for (let y = 0; y < this.rows; y++) {
            for(let x = 0; x < this.cols; x++) {
                this.grid.push(new Square(x, y, this.square_size));
            }
        }
    }

    get_square(x, y) {
        if (x < 0 || x >= this.cols || y < 0 || y >= this.rows) {
            return undefined;
        }
        return this.grid[y * this.cols + x];
    }

    get_random_neighbor(x, y, visited=false) {
        let neighbors = [];

        let top = this.get_square(x, y - 1);
        let right = this.get_square(x + 1, y);
        let bottom = this.get_square(x, y + 1);
        let left = this.get_square(x - 1, y);

        if (top !== undefined && top.isVisited === visited) {
            neighbors.push(top);
        }
        if (right !== undefined && right.isVisited === visited) {
            neighbors.push(right);
        }
        if (bottom !== undefined && bottom.isVisited === visited) {
            neighbors.push(bottom);
        }
        if (left != undefined && left.isVisited === visited) {
            neighbors.push(left);
        }

        if (neighbors.length > 0) {
            return neighbors[Math.floor(Math.random() * neighbors.length)];
        } else {
            return undefined;
        }
    }

    create_maze_randomly() {
        let x = this.player.location.getX();
        let y = this.player.location.getY();
        this.player.location.set_visited();
        this.nextSq = this.get_random_neighbor(x, y);
        
        if (this.nextSq !== undefined) {
            this.nextSq.isVisited = true;
            this.stack.push(this.nextSq);

            // Removing walls
            if (this.player.location.getX() - this.nextSq.getX() === -1) { // Going Right
                this.player.location.remove_right_wall();
            } else if (this.player.location.getX() - this.nextSq.getX() === 1) { // Going Left
                this.nextSq.remove_right_wall();
            } else if (this.player.location.getY() - this.nextSq.getY() === -1) { // Going Down
                this.nextSq.remove_top_wall();
            } else if (this.player.location.getY() - this.nextSq.getY() === 1) { // Going Up
                this.player.location.remove_top_wall();
            }
            
            this.player.set_location(this.nextSq);
        }  else { // Backtracking
            if (this.stack.length <= 2) {
                this.mazeReady = true;
                this.exit_square = this.longest_path[this.longest_path.length-1];
            }
            if (this.longest_path.length < this.stack.length) {
                this.longest_path = [...this.stack];    
            }
            this.stack.pop();
            this.player.set_location(this.stack[this.stack.length - 1]);
        }
    }

    deleteWorld() {
        //TODO:
    }
}