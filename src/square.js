class Square {
    constructor(x, y, sq_size) {
        this.top_wall = true;
        this.right_wall = true;

        this.isVisited = false;
        
        this.x = x;
        this.y = y;
        this.size = sq_size;

        this.color = "white"; // default color is white
    }

    getSize()  { return this.size; }
    getX() { return this.x; }
    getY() { return this.y; }

    is_top_wall() { return this.top_wall; }
    is_right_wall() { return this.right_wall; }

    remove_top_wall() { this.top_wall = false; }
    remove_right_wall() { this.right_wall = false; }

    getColor() { return this.color; }
    setColor(r, g, b) { 
        this.color = `rgb(${r}, ${g}, ${b})`;
    }

    set_visited() { this.isVisited = true; }
}