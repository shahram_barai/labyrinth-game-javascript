
class Gui {
    constructor(world) {
        this.canvas = document.getElementById('gameCanvas');
        this.context = canvas.getContext("2d");
        
        this.world = world;
        this.sq_size = world.get_square_size();
        this.maze_width = world.get_cols() * this.sq_size;
        this.maze_height = world.get_rows() * this.sq_size;

        this.game_bordes_x = 0;
        this.game_bordes_y = 0;
        this.wall_thickness = 4;
    }

    draw_square_background(x, y, color="with") {
        this.context.fillStyle = color;
        this.context.fillRect(x, y, this.sq_size, this.sq_size);
    }

    draw_square_walls(x, y, square, color="black") {
        let line_width = this.wall_thickness / 2;
        this.context.lineWidth = this.wall_thickness;
        this.context.lineJoin = "miter";
        this.context.strokeStyle = color;
        this.context.beginPath();
        
        if (square.is_top_wall() === true) {
            this.context.moveTo(x, y - line_width);
            this.context.lineTo(x + this.sq_size, y - line_width);
        }

        if (square.is_right_wall() === true) {
            this.context.moveTo(x + this.sq_size - line_width, y - line_width);
            this.context.lineTo(x + this.sq_size - line_width, y + this.sq_size - line_width);
        }

        this.context.closePath();
        this.context.stroke();

    }

    draw_player() { // Direction: 0-Up, 1-Right, 2-Down, 3-Left
        let _x = this.world.get_player_location().getX();
        let _y = this.world.get_player_location().getY();
    
        let _player_size = this.sq_size * 0.60;

        let _mid_x = (_x * this.sq_size) + ((this.sq_size - this.wall_thickness) / 2) - (_player_size / 2);
        let _mid_y = (_y * this.sq_size) + ((this.sq_size - this.wall_thickness) / 2) - (_player_size / 2);
        context.fillStyle = "red";
        context.fillRect(_mid_x, _mid_y, _player_size, _player_size);
    }

    draw_maze_world() {
        let x = 0; let y = 0; let _square = 0;

        // Draw START square
        _square = this.world.get_start_square();
        x = _square.getX() * this.sq_size; y = _square.getY() * this.sq_size;
        this.draw_square_background(x, y, "gray");

        // Draw EXIT square
        _square = this.world.get_exit_square();
        x = _square.getX() * this.sq_size; y = _square.getY() * this.sq_size;
        this.draw_square_background(x, y, "green");

        // Draw grid: Cells
        //draw_square_background(x, y);
        for(y = 0; y < this.world.get_rows(); y++) {
            for(x = 0; x < this.world.get_cols(); x++) {
                _square = this.world.get_square(x, y);
                this.draw_square_walls(x * this.sq_size, y * this.sq_size, _square);
            }
        }

        // Draw grid: Border
        this.context.lineWidth = 4;
        this.context.strokeStyle = "black";
        this.context.strokeRect(0, 0, this.maze_width, this.maze_height);

        // Draw player
        this.draw_player();
    }
}