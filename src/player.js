class Player {
    constructor(name, world) {
        this.name = name;
        this.world = world;
        this.location;
    }

    getName() { return this.name; }
    setName(name) { this.name = name; }

    get_location() { return this.location; }
    set_location(square) { this.location = square; }

    // dierection: 0 - up, 1 - right, 2 - down, 3 - left
    tryToMove(direction) {
        let x = this.location.getx();
        let y = this.location.getY();

        if (direction === 0 && !this.world.get_squere(x, y).isTopWall()) {
            x = x + 1;
        } else if  (direction === 1 && !this.world.get_squere(x, y).isRightWall()) {
            y = y + 1;
        } else if (direction === 2 && !this.world.get_squere(x, y+1).isTopWall()) {
            x = x - 1;
        } else if (direction === 3 && !this.world.get_squere(x-1, y).isRightWall())  {
            y = y - 1;
        }

        this.setLocation(this.world.get_squere(x, y));
    }
}