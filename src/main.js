// CANVAS
let canvas;
let context;
const updateInterval = 100;
let i = 0;

// Labyrint World
let test_world;
let gui;

function drawContext() {
    if (!test_world.mazeReady) {
        test_world.create_maze_randomly();
        context.clearRect(0, 0, canvas.width, canvas.height);
        gui.draw_maze_world();
    }
}

function resize() {
    let box = canvas.getBoundingClientRect();
    canvas.width = box.width;
    canvas.height = box.height;
}

function render() {
    animationRequest = requestAnimationFrame(() => render());

    let currentTime = performance.now();
    if (!render.lastRender || currentTime - render.lastRender >= updateInterval) {
        i = i + 10;
        drawContext();
        render.lastRender = currentTime;
    }
}

function main() {
    canvas = document.getElementById("canvas");
    context = canvas.getContext('2d');
    resize();
    
    test_world = new LabyrinthWorld(canvas.width, canvas.height, 20);
    gui = new Gui(test_world); 

    gui.draw_maze_world();
    
    render();
}

window.addEventListener("load", () => main());